module ApplicationHelper

	require 'socket'
	include Socket::Constants

	# Methods created for Ex 1

	def GET(host)		

		port = 80

		# Creating request
		request = "GET HTTP/1.0\r\n\r\n"

		# Creating new socket
		socket = Socket.new(AF_INET, SOCK_STREAM, 0)
		# Configuring connection params in socket (port, URL)
		sockaddr = Socket.sockaddr_in(port, host)
		# Starting connection and sending request.
		socket.connect(sockaddr)
		socket.send(request, 0)
		
		# Getting response, closing socket and redirecting user to the results view
		response = socket.recv(4095)
		puts response

		socket.close

		return response.to_s
		
	end

	# Methods created for Ex 2

	def ComputePrimesDescending(max_number)

		array = []
		array << 1
		array << 2		
		for i in 3..max_number

			is_prime = true

			for j in 2..i-1
				if i % j == 0
					is_prime = false
					break
				end
			end

			if is_prime
				array << i
				is_prime = true
				puts i
			end
		end

		#puts array.reverse
		return array.reverse
	end
end
