class HomeController < ApplicationController

  include ApplicationHelper
  
  def index
  end

  def ex_one
  	response = GET("www.mcafee.com")

  	redirect_to result_path(:response => response, :ex => "1")
  end

  def ex_two
  	#nothing to do here
  end

  def ex_two_form
  	max_number = params[:max_number].to_i
  	#array = ComputePrimesDescending(max_number)
  	redirect_to primes_path(:ex => "2", :max_number => max_number.to_s)
  end

  def primes

  	max_number = params[:max_number].to_i
  	@array = ComputePrimesDescending(max_number)

  end

end
